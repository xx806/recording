from math import ceil,sqrt

def modo(a,b):
    res=a%b
    if res:
        return res
    else:
        return b

def factorise(n):
    map={}
    t=n
    while t>=2:
        s=2
        bound=sqrt(t)
        while 1:
            if s>bound:
                if t in map:
                    map[t]+=1
                else:
                    map[t]=1
                return map
            if t%s==0:
                t=t//s
                if s in map:
                    map[s]+=1
                else:
                    map[s]=1
                break
            s+=1
    return map

def phi(n,bmap):
    res=1
    cp=0
    map=factorise(n)
    for y in bmap:
        if (y in map):
            cur=ceil(map[y]/bmap[y])
            if cur>cp:
                cp=cur
    for x in map:
        res*=((x**map[x])-(x**(map[x]-1)))
    return res,cp


def tower(base, h, m):
    bmap=factorise(base)
    if m==1:
        return 0
    if base==1 or h==0:
        return 1
    if h==1:
        return base%m
    phis=[(m,-1)]
    for x in range (h-1):       
        a=phi(phis[x][0],bmap)       

        if a[0]==0:
            break
        phis.append(a)
    phis.reverse()

    
    temp=1
    for y in range(len(phis)-1):
        compute=base**temp
        copy=phis[y][1]
        period=phis[y][0]
        if compute<period+copy:
            temp=compute
        else:
            temp=modo((compute-copy+1),period)+copy-1

    return (base**temp)%m

print(tower(82973, 38281, 614))

