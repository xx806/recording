from math import ceil
from math import sqrt
from collections import Counter
def modo(a,b):
    res=a%b
    if res:
        return res
    else:
        return b

def factorise(n):
    map={}
    t=n
    while t>=2:
        s=2
        bound=sqrt(t)
        while 1:
            if s>bound:
                if t in map:
                    map[t]+=1
                else:
                    map[t]=1
                return map
            if t%s==0:
                t=t//s
                if s in map:
                    map[s]+=1
                else:
                    map[s]=1
                break
            s+=1
    return map
#print(factorise(97))

def phi_gen(n,bmap,map):
    cp=0
    while True:
        n=1
        for y in bmap:
                if (y in map):
                    cur=ceil(map[y]/bmap[y])
                    if cur>cp:
                        cp=cur
        newmap=Counter()
        for z in map:
            n*=((z**map[z])-(z**(map[z]-1)))
            if map[z]>1:
                newmap[z]=map[z]-1
            newmap=Counter(factorise(z-1))+newmap

        map=dict(newmap)
        yield n,cp


def tower(base, h, m):
    bmap=factorise(base)
    if m==1:
        return 0
    if base==1 or h==0:
        return 1
    if h==1:
        return base%m
    gen=phi_gen(m,bmap,factorise(m))
    phis=[]
    for x in range (h-1):       
        a=next(gen)       
        if a[0]==0:
            break
        phis.append(a)
    phis.reverse()
    
    temp=1
    for y in range(len(phis)):
        compute=base**temp
        copy=phis[y][1]
        period=phis[y][0]
        if compute<period+copy:
            temp=compute
        else:
            temp=modo((compute-copy+1),period)+copy-1

    return (base**temp)%m

print(tower(4, 3, 10))


print(tower(2,6,1001))

print(tower(82973, 38281, 614))
